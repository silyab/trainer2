import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import {DataService} from "./services/data.service";
import {RouterModule, Routes} from "@angular/router";
import { ImageComponent } from './components/trainer/components/image/image.component';
import { FormComponent } from './components/trainer/components/form/form.component';
import { ListenerComponent } from './components/trainer/components/listener/listener.component';
import { Form2Component } from './components/trainer/components/form2/form2.component';
import { IaTrainerComponent } from './components/ia-trainer/ia-trainer.component';
import { PictureComponent } from './components/ia-trainer/components/picture/picture.component';

const appRoutes: Routes = [
  { path: 'train/:bijou_id', component: TrainerComponent },
  { path: 'train', component: TrainerComponent },
  { path: 'iatrain', component: IaTrainerComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    TrainerComponent,
    ImageComponent,
    FormComponent,
    ListenerComponent,
    Form2Component,
    IaTrainerComponent,
    PictureComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
