import { Injectable } from '@angular/core';
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  //ip = '192.168.0.21:5000';
  ip = '157.159.80.102:5000';
  basic_header = new Headers();

  constructor(private _http: Http) {
    this.basic_header.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
    this.basic_header.append('dataType-Type', 'json');
  }

  getNextBijou(id){
    const url = 'http://' + this.ip + '/train/' + id;

    return this._http
      .get(url)
      .map(res => res.json());
  }

  setBijou(id, data){
    const url = 'http://' + this.ip + '/train/' + id;

    return this._http
      .post(url, JSON.stringify(data), {headers: this.basic_header})
      .map(res => res.json());
  }

  getIaTrain(){
    const url = 'http://' + this.ip + '/iatrain';

    return this._http
      .get(url)
      .map(res => res.json());
  }

  setIaTrain(data){
    const url = 'http://' + this.ip + '/iatrain';

    return this._http
      .post(url, JSON.stringify(data), {headers: this.basic_header})
      .map(res => res.json());
  }

}
