import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-ia-trainer',
  templateUrl: './ia-trainer.component.html',
  styleUrls: ['./ia-trainer.component.css']
})
export class IaTrainerComponent implements OnInit {

  keys = [];
  focus;
  data;
  response;
  field = 'taille';

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.init();
  }

  init(){
    this.dataService.getIaTrain().subscribe((response) => {
      this.data = response;
      console.log(response);
      this.init_keys();
    });
  }

  init_keys(){
    this.keys = [];
    this.response = '';
    for (let elm in this.data){
      if (this.data.hasOwnProperty(elm))
        this.keys.push(elm);
    }
    this.focus = this.keys[0];
  }

  change(i, old_key, key){
    this.data[key].push(this.data[old_key][i]);
    this.data[old_key].splice(i, 1);
  }

  save(){
    let data = [];

    for (let key of this.keys){
      for (let elm of this.data[key]){
        let value = {'id': elm['id']};
        value[this.field] = key;
        data.push(value);
      }
    }

    this.dataService.setIaTrain(data).subscribe((response) => {
      this.response = response;
    });
  }

}
