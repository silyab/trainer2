import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IaTrainerComponent } from './ia-trainer.component';

describe('IaTrainerComponent', () => {
  let component: IaTrainerComponent;
  let fixture: ComponentFixture<IaTrainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IaTrainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IaTrainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
