import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.css']
})
export class PictureComponent implements OnInit {

  @Input('parent') parent;
  @Input('data') data;
  @Input('curr_key') curr_key;
  @Input('i') i;
  open_menu = false;

  constructor() { }

  ngOnInit() {
  }

}
