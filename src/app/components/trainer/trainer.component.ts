import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from "../../services/data.service";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  bijou = {};
  previous_bijou = {};
  previous_id = 0;
  response = '';
  values = {
    'couleur_metal': {1: 'argente', 2: 'dore', 3: 'rose', 4: 'noir', 5: 'autre'},
    'couleurs': {1: 'transparent', 2: 'noir', 3: 'blanc', 4: 'gris', 5: 'beige', 6: 'marron', 7: 'marron_clair',
      8: 'rouge', 9: 'rouge_borde', 10: 'rose', 11: 'rose_clair', 12: 'bleu', 13: 'bleu_clair', 14: 'bleu_vert',
      15: 'violet', 16: 'vert', 17: 'orange', 18: 'jaune', 19: 'multi'},
    'collier_longueur': {1: 'ras_du_cou', 2: 'poitrine', 3: 'decolte', 4: 'sautoir'},
    'collier_forme': {1: 'rond', 2: 'v', 3: 'autre'},
    'bague_monture': {1: 'anneau', 2: 'basse', 3: 'haute'},
    'boucle_forme': {1: 'rond', 2: 'carre', 3: 'droit', 4: 'autre'},
    'styles': {1: 'simple', 2: 'moderne', 3: 'classique', 4: 'mystique', 5: 'rock', 6: 'enfantin', 7: 'floral',
      8: 'animal', 9: 'antique', 10: 'boheme', 11: 'ancien', 12: 'celtique', 13: 'extravagant'}
  };
  ordre = {
    //'Bague': ['couleur_metal', 'couleurs', 'bague_monture', 'taille', 'occasion', 'qualite', 'styles'],
    'Bague': ['styles'],
    'Bracelet': ['couleur_metal', 'couleurs', 'taille', 'occasion', 'qualite', 'styles'],
    'Collier': ['couleur_metal', 'couleurs', 'collier_longueur', 'collier_forme', 'taille', 'occasion', 'qualite', 'styles'],
    'Boucle_oreil': ['couleur_metal', 'couleurs', 'boucle_forme', 'taille', 'occasion', 'qualite', 'styles'],
  };
  focus;

  @ViewChild('listener') listener;

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService) {
  }

  ngOnInit() {
    // choix id
    this.activatedRoute.params.subscribe((params: Params) => {
      (params['bijou_id'] !== undefined) ? this.get_next(params['bijou_id']) : this.get_next(0);
    });
  }

  get_next(bijou_id) {
    this.dataService.getNextBijou(bijou_id).subscribe((response) => {
      this.bijou = response;
      this.change_focus(this.ordre[this.bijou['categorie']][0]);
    });
  }

  send(mode){
    if (mode=='save'){
      this.bijou['treated'] = true;
      this.bijou['banned'] = false;
    }
    else if (mode=='ban'){
      this.bijou['treated'] = false;
      this.bijou['banned'] = true;
    }
    let data = this.prepData();
    this.dataService.setBijou(this.bijou['id'], data).subscribe((response) => {
      this.response = response;
      console.log(response);
      this.get_next(0);
    });
  }

  change_focus(key) {
    this.focus = key;
    let default_value = 1;
    if (['taille', 'occasion', 'qualite'].indexOf(key) >= 0){
      default_value = this.bijou['data'].hasOwnProperty(key) ? this.bijou['data'][key] : 50;
    }
    if (['couleurs', 'styles'].indexOf(key) >= 0){
      default_value = 0;
    }
    this.listener.changeFocus(default_value, toArray(this.values[key]));
  }

  updateFromListener(value){
    if (value == 0){
      this.nextFocus();
    }
    else if (value == 'save'){
      this.send('save');
    }
    else if (['taille', 'occasion', 'qualite'].indexOf(this.focus) >= 0){
      if (0 <= value && value <= 100){
        this.bijou['data'][this.focus] = value;
        this.nextFocus();
      }
      else {
        this.nextFocus(true);
      }
    }
    else {
      if(this.values[this.focus].hasOwnProperty(value)){
        if (['couleurs', 'styles'].indexOf(this.focus) >= 0){
          let value_to_set = this.values[this.focus][value];
          if (this.bijou['data'][this.focus].indexOf(value_to_set) == -1) {
            this.bijou['data'][this.focus].push(value_to_set);
          }
          this.nextFocus(true);
        }
        else{
          this.bijou['data'][this.focus] = this.values[this.focus][value];
          this.nextFocus();
        }
      }
      else {
        this.nextFocus(true);
      }
    }
  }

  nextFocus(reset=false){
    let next = false;
    for (let key of this.ordre[this.bijou['categorie']]){
      if (next) {
        this.change_focus(key);
        return
      }
      if (key === this.focus){
        if (reset){
          this.change_focus(key);
          return
        }
        else{
          next = true;
        }
      }
    }

    // Si dernier
    this.send('save');
  }

  remove(key, value){
    let index = this.bijou['data'][key].indexOf(value);
    if (index >= 0){
      this.bijou['data'][key].splice(index, 1);
    }
  }

  updateImage(key, key2, value){
    this.bijou['images'][key][key2] = value;
  }

  prepData(){
    let key_to_send = ['couleur_metal', 'couleurs', 'collier_longueur', 'collier_forme', 'taille', 'occasion',
      'qualite', 'styles', 'bague_monture', 'boucle_forme'];

    let res = {'images': this.bijou['images'], 'data': {},
      'treated': this.bijou['treated'], 'banned': this.bijou['banned']};

    this.previous_bijou = res['data'];
    this.previous_id = this.bijou['id'];

    for (let key of key_to_send){
      if (this.bijou['data'].hasOwnProperty(key)){
        res['data'][key] = this.bijou['data'][key];
      }
    }

    return res
  }

  resetPrevious(){
    for (let key in this.previous_bijou){
      this.bijou['data'][key] = this.previous_bijou[key];
    }
  }
}

function toArray(dict){
  if (!dict) { return null}
  let res = [];
  for (let i = 1; dict.hasOwnProperty(i); i++){
    res.push(dict[i]);
  }
  return res;
}
