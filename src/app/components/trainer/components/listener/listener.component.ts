import {Component, HostListener, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-listener',
  templateUrl: './listener.component.html',
  styleUrls: ['./listener.component.css']
})
export class ListenerComponent implements OnInit {

  @Input('parent') parent;
  buffer;
  buffer_default;
  values;

  clsse_keys = ['couleur_metal', 'couleurs', 'collier_longueur', 'collier_forme', 'bague_monture', 'boucle_forme', 'styles'];

  constructor() { }

  ngOnInit() {
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event === undefined || event.key === 'Enter'){
      this.parent.updateFromListener(this.buffer);
      this.buffer_default = true;
      return
    }

    if (event.key in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]){
      if (this.buffer_default) {
        this.buffer = parseInt(event.key);
        this.buffer_default = false;
      }
      else {
        this.buffer = parseInt(this.buffer.toString() + event.key);
      }
    }

    if (event.key == '-'){
      this.buffer = 1;
      this.buffer_default = true;
    }

    if (event.key == 's'){
      this.parent.updateFromListener('save');
      this.buffer_default = true;
    }
  }

  changeFocus(default_value, values){
    this.buffer = default_value;
    this.values = values;
    this.buffer_default = true;
  }

}
