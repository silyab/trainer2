import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input('parent') parent;
  @Input('key') key;

  constructor() { }

  ngOnInit() {
  }

}
