import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  @Input('parent') parent;
  @Input('url') url;
  @Input('image_name') image_name;
  @Input('image') image;
  @Input('mini') mini;

  constructor() { }

  ngOnInit() {
  }

}
