import { Trainer2Page } from './app.po';

describe('trainer2 App', () => {
  let page: Trainer2Page;

  beforeEach(() => {
    page = new Trainer2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
